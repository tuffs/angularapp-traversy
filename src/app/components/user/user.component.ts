import { Component } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html',
  styleUrls: ['user.component.css']
})

export class UserComponent {
  // Properties
  firstName: string;
  lastName: string;
  age: number;
  address;

  constructor() {
    this.firstName = 'John';
    this.lastName = 'Doe';
    this.age = 30;
    this.address = {
      street: '40 Main St.',
      city: 'Boston',
      state: 'MA'
    }
  }

}
